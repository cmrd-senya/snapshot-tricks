const {
  GLib
 } = imports.gi

var topLevelAwait = (asyncFunc) => {
  let resultBox = null
  const loop = GLib.MainLoop.new(null, false);

  asyncFunc().then(
    (result) => {
      resultBox = result
      loop.quit()
    }
  )

  loop.run()
  return resultBox
}
