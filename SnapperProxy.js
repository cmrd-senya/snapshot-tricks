const {
  Gio
 } = imports.gi

const {
  DBusProxy: { makeProxyWrapper }
 } = Gio

 const SnapperProxyDescription = `
 <node name='/org/opensuse/Snapper'>
  <interface name='org.opensuse.Snapper'>
    <signal name='ConfigCreated'>
      <arg name='config-name' type='s'/>
    </signal>
    <signal name='ConfigModified'>
      <arg name='config-name' type='s'/>
    </signal>
    <signal name='ConfigDeleted'>
      <arg name='config-name' type='s'/>
    </signal>
    <signal name='SnapshotCreated'>
      <arg name='config-name' type='s'/>
      <arg name='number' type='u'/>
    </signal>
    <signal name='SnapshotModified'>
      <arg name='config-name' type='s'/>
      <arg name='number' type='u'/>
    </signal>
    <signal name='SnapshotsDeleted'>
      <arg name='config-name' type='s'/>
      <arg name='number' type='u'/>
    </signal>
    <method name='ListConfigs'>
      <arg name='configs' type='a(ssa{ss})' direction='out'/>
    </method>
    <method name='GetConfig'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='data' type='(ssa{ss})' direction='out'/>
    </method>
    <method name='SetConfig'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='data' type='a{ss}' direction='in'/>
    </method>
    <method name='CreateConfig'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='subvolume' type='s' direction='in'/>
      <arg name='fstype' type='s' direction='in'/>
      <arg name='template-name' type='s' direction='in'/>
    </method>
    <method name='DeleteConfig'>
      <arg name='config-name' type='s' direction='in'/>
    </method>
    <method name='LockConfig'>
      <arg name='config-name' type='s' direction='in'/>
    </method>
    <method name='UnlockConfig'>
      <arg name='config-name' type='s' direction='in'/>
    </method>
    <method name='ListSnapshots'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='snapshots' type='a(uquxussa{ss})' direction='out'/>
    </method>
    <method name='ListSnapshotsAtTime'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='begin' type='x' direction='in'/>
      <arg name='end' type='x' direction='in'/>
      <arg name='snapshots' type='a(uquxussa{ss})' direction='out'/>
    </method>
    <method name='GetSnapshot'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='number' type='u' direction='in'/>
      <arg name='type' type='(uquxussa{ss})' direction='out'/>
    </method>
    <method name='SetSnapshot'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='number' type='u' direction='in'/>
      <arg name='description' type='s' direction='in'/>
      <arg name='cleanup' type='s' direction='in'/>
      <arg name='userdata' type='a{ss}' direction='in'/>
    </method>
    <method name='CreateSingleSnapshot'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='description' type='s' direction='in'/>
      <arg name='cleanup' type='s' direction='in'/>
      <arg name='userdata' type='a{ss}' direction='in'/>
      <arg name='number' type='u' direction='out'/>
    </method>
    <method name='CreateSingleSnapshotV2'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='parent-number' type='u' direction='in'/>
      <arg name='read-only' type='b' direction='in'/>
      <arg name='description' type='s' direction='in'/>
      <arg name='cleanup' type='s' direction='in'/>
      <arg name='userdata' type='a{ss}' direction='in'/>
      <arg name='number' type='u' direction='out'/>
    </method>
    <method name='CreateSingleSnapshotOfDefault'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='read-only' type='b' direction='in'/>
      <arg name='description' type='s' direction='in'/>
      <arg name='cleanup' type='s' direction='in'/>
      <arg name='userdata' type='a{ss}' direction='in'/>
      <arg name='number' type='u' direction='out'/>
    </method>
    <method name='CreatePreSnapshot'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='description' type='s' direction='in'/>
      <arg name='cleanup' type='s' direction='in'/>
      <arg name='userdata' type='a{ss}' direction='in'/>
      <arg name='number' type='u' direction='out'/>
    </method>
    <method name='CreatePostSnapshot'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='pre-number' type='u' direction='in'/>
      <arg name='description' type='s' direction='in'/>
      <arg name='cleanup' type='s' direction='in'/>
      <arg name='userdata' type='a{ss}' direction='in'/>
      <arg name='number' type='u' direction='out'/>
    </method>
    <method name='DeleteSnapshots'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='numbers' type='au' direction='in'/>
    </method>
    <method name='GetDefaultSnapshot'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='valid' type='b' direction='out'/>
      <arg name='number' type='u' direction='out'/>
    </method>
    <method name='GetActiveSnapshot'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='valid' type='b' direction='out'/>
      <arg name='number' type='u' direction='out'/>
    </method>
    <method name='CalculateUsedSpace'>
      <arg name='config-name' type='s' direction='in'/>
    </method>
    <method name='GetUsedSpace'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='number' type='u' direction='in'/>
      <arg name='sued-space' type='u' direction='out'/>
    </method>
    <method name='MountSnapshot'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='number' type='u' direction='in'/>
      <arg name='user-request' type='b' direction='in'/>
      <arg name='path' type='s' direction='out'/>
    </method>
    <method name='UmountSnapshot'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='number' type='u' direction='in'/>
      <arg name='user-request' type='b' direction='in'/>
    </method>
    <method name='GetMountPoint'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='number' type='u' direction='in'/>
      <arg name='path' type='s' direction='out'/>
    </method>
    <method name='CreateComparison'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='number1' type='u' direction='in'/>
      <arg name='number2' type='u' direction='in'/>
      <arg name='num-files' type='u' direction='out'/>
    </method>
    <method name='DeleteComparison'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='number1' type='u' direction='in'/>
      <arg name='number2' type='u' direction='in'/>
    </method>
    <method name='GetFiles'>
      <arg name='config-name' type='s' direction='in'/>
      <arg name='number1' type='u' direction='in'/>
      <arg name='number2' type='u' direction='in'/>
      <arg name='files' type='a(su)' direction='out'/>
    </method>
    <method name='Sync'>
      <arg name='config-name' type='s' direction='in'/>
    </method>
  </interface>
</node>
`

const SNAPPER_BUS_NAME = 'org.opensuse.Snapper'
const SNAPPER_OBJECT_NAME = '/org/opensuse/Snapper'

var SnapperProxy = makeProxyWrapper(SnapperProxyDescription)
var SnapperProxyInstance = new SnapperProxy(Gio.DBus.system, SNAPPER_BUS_NAME, SNAPPER_OBJECT_NAME)
