#!/usr/bin/gjs

const {
  GLib,
  Gio
 } = imports.gi

 const {
  DBusProxy,
  BusType,
  DBusObjectManagerClient
} = Gio

const ByteArray = imports.byteArray

const UDISKS_BUS_NAME = 'org.freedesktop.UDisks2'
const UDISKS_OBJECT_NAME = '/org/freedesktop/UDisks2'
const UDISKS_FILESYSTEM_IFACE = 'org.freedesktop.UDisks2.Filesystem'

const loop = GLib.MainLoop.new(null, false);

const createManagerClient = async () => {
  return new Promise((resolve, reject) => {
    DBusObjectManagerClient.new_for_bus(
      BusType.SYSTEM,
      Gio.DBusObjectManagerClientFlags.NONE,
      UDISKS_BUS_NAME,
      UDISKS_OBJECT_NAME,
      null,
      null,
      resolve
    )
  })
}


createManagerClient().then(async (manager) => {
  const filesystems = manager.get_objects().map((obj) =>
    (obj.get_interface(UDISKS_FILESYSTEM_IFACE))
  ).filter(Boolean)

  const rootFileSystem = filesystems.find((filesystem) => {
    // log(filesystem.get_name())
    const mountPoints = filesystem.get_cached_property('MountPoints')
    // log(mountPoints.print(true))
    const path = ByteArray.toString(mountPoints.get_child_value(0).get_bytestring())
    return path === '/'
  })
  log(rootFileSystem.get_object_path())

  loop.quit();
})




loop.run()
