#!/usr/bin/gjs

imports.searchPath.unshift('.')

const { UDisksBtrfsProxy } = imports.UDisksBtrfsProxy

const UDISKS_BUS_NAME = 'org.freedesktop.UDisks2'
const UDISKS_OBJECT_NAME = '/org/freedesktop/UDisks2/block_devices/sda1'

const proxyInstance = new UDisksBtrfsProxy(UDISKS_OBJECT_NAME)

const res = proxyInstance.GetSubvolumesSync(
  false, []
)

log(res[0][9][2])
