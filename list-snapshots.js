#!/usr/bin/gjs

const {
  GLib,
  Gio
 } = imports.gi

 const {
  DBusProxy,
  BusType,
  DBusObjectManagerClient
} = Gio

const SNAPPER_BUS_NAME = 'org.opensuse.Snapper'
const SNAPPER_OBJECT_NAME = '/org/opensuse/Snapper'
const SNAPPER_INTERFACE_NAME = 'org.opensuse.Snapper'

const loop = GLib.MainLoop.new(null, false);

const createProxy = async () => {
  return new Promise((resolve, reject) => {
    DBusProxy.new_for_bus(
      BusType.SYSTEM,
      Gio.DBusProxyFlags.G_DBUS_PROXY_FLAGS_NONE,
      null,
      SNAPPER_BUS_NAME,
      SNAPPER_OBJECT_NAME,
      SNAPPER_INTERFACE_NAME,
      null,
      resolve
    )
  })
}

const SNAPPER_CONFIG = 'root'

createProxy().then(async (proxy) => {
  const parameters = GLib.Variant.new_tuple([
    GLib.Variant.new_string(SNAPPER_CONFIG)
  ])

  // // async
  // const result = new Promise((resolve, reject) => {
  //   proxy.call(
  //     'ListSnapshots',
  //     parameters,
  //     Gio.DBusCallFlags.NONE,
  //     -1,
  //     null,
  //     resolve
  //   )
  // })

  // await result

  const result = proxy.call_sync(
    'ListSnapshots',
    parameters,
    Gio.DBusCallFlags.NONE,
    -1,
    null
  )
  if (result) {
    log(result.print(true))
  }
  loop.quit();
})




loop.run()
