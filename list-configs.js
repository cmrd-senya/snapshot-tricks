#!/usr/bin/gjs

const {
  GLib,
  Gio
 } = imports.gi

 const {
  DBusProxy,
  BusType,
  DBusObjectManagerClient
} = Gio

const SNAPPER_BUS_NAME = 'org.opensuse.Snapper'
const SNAPPER_OBJECT_NAME = '/org/opensuse/Snapper'
const SNAPPER_INTERFACE_NAME = 'org.opensuse.Snapper'

const loop = GLib.MainLoop.new(null, false);

const createProxy = async () => {
  return new Promise((resolve, reject) => {
    DBusProxy.new_for_bus(
      BusType.SYSTEM,
      Gio.DBusProxyFlags.G_DBUS_PROXY_FLAGS_NONE,
      null,
      SNAPPER_BUS_NAME,
      SNAPPER_OBJECT_NAME,
      SNAPPER_INTERFACE_NAME,
      null,
      resolve
    )
  })
}

createProxy().then(async (proxy) => {
  // dbus-send --system --print-reply --dest=org.opensuse.Snapper  / org.opensuse.Snapper.ListConfigs
  const result = proxy.call_sync(
    'ListConfigs',
    null,
    Gio.DBusCallFlags.NONE,
    -1,
    null
  )
  log(result.print(true))
  loop.quit();
})




loop.run()
