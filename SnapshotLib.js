
var createRWRootSnapshot = (btrfsInstance, { id }) => {
  const now = (new Date).toISOString()
  const source = `/.snapshots/${id}/snapshot`
  const dest = `/mountable_roots/snap-${id}-at-${now}`

  btrfsInstance.CreateSnapshotSync(
    source,
    dest,
    false,
    []
  )
  return dest
}

var recursivelySnapshotToNewRoot = (btrfsInstance, newRoot) => {
  const [snapshots] = btrfsInstance.GetSubvolumesSync(
    false, []
  )

  for (snapshot in snapshots) {
    log(snapshot)
  }
}
