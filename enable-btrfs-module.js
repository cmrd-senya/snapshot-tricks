#!/usr/bin/gjs

const {
  GLib,
  Gio
 } = imports.gi

 const {
  DBusProxy,
  BusType,
  DBusObjectManagerClient
} = Gio

const ByteArray = imports.byteArray

const UDISKS_BUS_NAME = 'org.freedesktop.UDisks2'
const UDISKS_MANAGER_OBJECT_NAME = '/org/freedesktop/UDisks2/Manager'
const UDISKS_MANAGER_IFACE = 'org.freedesktop.UDisks2.Manager'

const MODULE_NAME = 'btrfs'

const loop = GLib.MainLoop.new(null, false);

const createProxy = async () => {
  return new Promise((resolve, reject) => {
    DBusProxy.new_for_bus(
      BusType.SYSTEM,
      Gio.DBusProxyFlags.G_DBUS_PROXY_FLAGS_NONE,
      null,
      UDISKS_BUS_NAME,
      UDISKS_MANAGER_OBJECT_NAME,
      UDISKS_MANAGER_IFACE,
      null,
      resolve
    )
  })
}
createProxy().then(async (proxy) => {
  const parameters = GLib.Variant.new_tuple([
    GLib.Variant.new_string(MODULE_NAME),
    GLib.Variant.new_boolean(true)
  ])

  try {
    const result = proxy.call_sync(
      'EnableModule',
      parameters,
      Gio.DBusCallFlags.NONE,
      -1,
      null
    )
    log(result.print(true))
  } catch (err) {
    logError(err)
  }
  loop.quit();
})


loop.run()
