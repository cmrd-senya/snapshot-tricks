const {
  GObject,
  Gio
 } = imports.gi

const {
  DBusProxy: { makeProxyWrapper }
 } = Gio

const BtrfsInterfaceDescription = `<node>
  <interface name="org.freedesktop.UDisks2.Filesystem.BTRFS">
    <method name="AddDevice">
      <arg type="o" name="device" direction="in"/>
      <arg type="a{sv}" name="options" direction="in"/>
    </method>
    <method name="RemoveDevice">
      <arg type="o" name="device" direction="in"/>
      <arg type="a{sv}" name="options" direction="in"/>
    </method>
    <method name="CreateSubvolume">
      <arg type="s" name="name" direction="in"/>
      <arg type="a{sv}" name="options" direction="in"/>
    </method>
    <method name="RemoveSubvolume">
      <arg type="s" name="name" direction="in"/>
      <arg type="a{sv}" name="options" direction="in"/>
    </method>
    <method name="GetSubvolumes">
      <arg type="b" name="snapshots_only" direction="in"/>
      <arg type="a{sv}" name="options" direction="in"/>
      <arg type="a(tts)" name="subvolumes" direction="out"/>
      <arg type="i" name="subvolumes_cnt" direction="out"/>
    </method>
    <method name="CreateSnapshot">
      <arg type="s" name="source" direction="in"/>
      <arg type="s" name="dest" direction="in"/>
      <arg type="b" name="ro" direction="in"/>
      <arg type="a{sv}" name="options" direction="in"/>
    </method>
    <method name="Repair">
      <arg type="a{sv}" name="options" direction="in"/>
    </method>
    <method name="Resize">
      <arg type="t" name="size" direction="in"/>
      <arg type="a{sv}" name="options" direction="in"/>
    </method>
    <method name="SetLabel">
      <arg type="s" name="label" direction="in"/>
      <arg type="a{sv}" name="options" direction="in"/>
    </method>
    <property type="s" name="label" access="read"/>
    <property type="s" name="uuid" access="read"/>
    <property type="t" name="num_devices" access="read"/>
    <property type="t" name="used" access="read"/>
  </interface>
</node>`

const UDISKS_BUS_NAME = 'org.freedesktop.UDisks2'

const UDisksBtrfsProxyBase = makeProxyWrapper(BtrfsInterfaceDescription)

var UDisksBtrfsProxy = class extends UDisksBtrfsProxyBase {
  constructor(objectPath) {
    super(Gio.DBus.system, UDISKS_BUS_NAME, objectPath)
  }
}
