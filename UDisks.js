
const {
  GLib,
  Gio
 } = imports.gi

 const {
  DBusProxy,
  BusType,
  DBusObjectManagerClient
} = Gio

const ByteArray = imports.byteArray

const UDISKS_BUS_NAME = 'org.freedesktop.UDisks2'
const UDISKS_OBJECT_NAME = '/org/freedesktop/UDisks2'
const UDISKS_FILESYSTEM_IFACE = 'org.freedesktop.UDisks2.Filesystem'

var createUDisksManagerClient = async () => {
  return new Promise((resolve, _reject) => {
    DBusObjectManagerClient.new_for_bus(
      BusType.SYSTEM,
      Gio.DBusObjectManagerClientFlags.NONE,
      UDISKS_BUS_NAME,
      UDISKS_OBJECT_NAME,
      null,
      null,
      resolve
    )
  })
}

const getFileSystems = (managerClient) => {
  return managerClient.get_objects().map((obj) =>
    (obj.get_interface(UDISKS_FILESYSTEM_IFACE))
  ).filter(Boolean)
}

var findRootObject = (managerClient) => {
  const filesystems = getFileSystems(managerClient)

  const rootFileSystem = filesystems.find((filesystem) => {
    const mountPoints = filesystem.get_cached_property('MountPoints')
    const path = ByteArray.toString(mountPoints.get_child_value(0).get_bytestring())
    return path === '/'
  })

  return rootFileSystem.get_object_path()
}
