imports.searchPath.unshift('.')

const { SnapperProxyInstance } = imports.SnapperProxy
const { UDisksBtrfsProxy } = imports.UDisksBtrfsProxy
const { createUDisksManagerClient, findRootObject } = imports.UDisks
const { topLevelAwait } = imports.asyncHelpers
const {
  createRWRootSnapshot,
  recursivelySnapshotToNewRoot
 } = imports.SnapshotLib

const CONFIG_NAME = 'root'
const SNAPSHOT_ID = 1

const requestSnapshots = () => {
  const [snapshots] = SnapperProxyInstance.ListSnapshotsSync(CONFIG_NAME)
  return snapshots
}

const findSnapshot = (snapshots, searchFunc) => {
  for (const snapshot of Object.values(snapshots)) {
    const [
      id,
      type,
      pre,
      date,
      user,
      description,
      cleanup,
      userdata
    ] = snapshot

    const snapshotObject = {
      id, type, pre, date, user, description, cleanup, userdata
    }

    if (searchFunc(snapshotObject)) { return snapshotObject }
  }

  return null
}


const snapshots = requestSnapshots()
const snapshot = findSnapshot(snapshots, ({id}) => (id === SNAPSHOT_ID))

if (!snapshot) {
  log(`Snapshot ${SNAPSHOT_ID} not found`)
} else {
  const managerClient = topLevelAwait(createUDisksManagerClient)
  const rootObjectPath = findRootObject(managerClient)
  const btrfsProxyInstance = new UDisksBtrfsProxy(rootObjectPath)

  const newRoot = createRWRootSnapshot(btrfsProxyInstance, snapshot)
  recursivelySnapshotToNewRoot(btrfsProxyInstance, newRoot)
}
