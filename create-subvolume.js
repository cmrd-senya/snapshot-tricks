#!/usr/bin/gjs

imports.searchPath.unshift('.')

const {
  GLib,
  Gio
 } = imports.gi

 const {
  DBusProxy,
  BusType,
  DBusObjectManagerClient
} = Gio

const { UDisksBtrfsProxy } = imports.UDisksBtrfsProxy

const ByteArray = imports.byteArray

const newSubvolumePath = '/mountable_roots'

const UDISKS_BUS_NAME = 'org.freedesktop.UDisks2'
const UDISKS_OBJECT_NAME = '/org/freedesktop/UDisks2'
const UDISKS_FILESYSTEM_IFACE = 'org.freedesktop.UDisks2.Filesystem'
const UDISKS_BTRFS_IFACE = 'org.freedesktop.UDisks2.Filesystem.BTRFS'

const loop = GLib.MainLoop.new(null, false);

const createSubvolume = async (manager) => {
  try {
    const filesystems = manager.get_objects().map((obj) =>
      (obj.get_interface(UDISKS_FILESYSTEM_IFACE))
    ).filter(Boolean)

    const rootFileSystem = filesystems.find((filesystem) => {
      // log(filesystem.get_name())
      const mountPoints = filesystem.get_cached_property('MountPoints')
      // log(mountPoints.print(true))
      const path = ByteArray.toString(mountPoints.get_child_value(0).get_bytestring())
      return path === '/'
    })

    const proxyInstance = new UDisksBtrfsProxy(rootFileSystem.get_object_path())
    const res = proxyInstance.CreateSubvolumeSync(newSubvolumePath, [])
    log(res)
    loop.quit()
    // const btrfsIface = manager.get_interface(rootFileSystem.get_object_path(), UDISKS_BTRFS_IFACE)
    // if (btrfsIface) {
    //   // const parameters = GLib.Variant.new_tuple([
    //   //   GLib.Variant.new_string('/my-new-subvolume'),
    //   //   GLib.Variant.new_tuple([])
    //   // ])
    //   const parameters = GLib.Variant.new_tuple([
    //     GLib.Variant.new_boolean(false),
    //     GLib.Variant.new_tuple([])
    //   ])

    //   const result = btrfsIface.call_sync(
    //     'GetSubvolumes',
    //     parameters,
    //     Gio.DBusCallFlags.NONE,
    //     100000000,
    //     null
    //   )
    //   log(result)
    // } else {
    //   log('Not a BTRFS root or BTRFS udisks2 module is not enabled!')
    // }
  } catch(err) {
    logError(err)
    loop.quit()
  }
}

const createManagerClient = async () => {
  return new Promise((resolve, reject) => {
    DBusObjectManagerClient.new_for_bus(
      BusType.SYSTEM,
      Gio.DBusObjectManagerClientFlags.NONE,
      UDISKS_BUS_NAME,
      UDISKS_OBJECT_NAME,
      null,
      null,
      resolve
    )
  })
}


createManagerClient().then(async (manager) => {
  await createSubvolume(manager)

  // const parameters = GLib.Variant.new_tuple([
  //   GLib.Variant.new_string(UDISKS_CONFIG)
  // ])

  // // async
  // const result = new Promise((resolve, reject) => {
  //   proxy.call(
  //     'ListSnapshots',
  //     parameters,
  //     Gio.DBusCallFlags.NONE,
  //     -1,
  //     null,
  //     resolve
  //   )
  // })

  // await result

  // const result = proxy.call_sync(
  //   'ListSnapshots',
  //   parameters,
  //   Gio.DBusCallFlags.NONE,
  //   -1,
  //   null
  // )
  // if (result) {
  //   log(result.print(true))
  // }
  // loop.quit();
})




loop.run()
